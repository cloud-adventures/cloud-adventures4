# VPC ID
output "eks_cluster_name" {
  description = "cluster name"
  value       = aws_eks_cluster.main.name
}

output "test_policy_arn" {
  value = aws_iam_role.test_oidc.arn
}
