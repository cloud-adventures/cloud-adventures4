# VPC ID
output "vpc_id" {
  description = "The ID of the VPC"
  value       = aws_vpc.main.id
}

# VPC CIDR blocks
output "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  value       = aws_vpc.main.cidr_block
}

# VPC Private Subnets
output "private_subnets" {
  description = "List of IDs of private subnets"
  //value       = [aws_subnet.private[0].cidr_block, aws_subnet.private[1].cidr_block]
  value = [for subnet in aws_subnet.private : subnet.cidr_block]
}

# VPC Public Subnets
output "public_subnets" {
  description = "List of IDs of public subnets"
  //value       = [aws_subnet.public[0].cidr_block, aws_subnet.public[1].cidr_block]
  value = [for subnet in aws_subnet.public : subnet.cidr_block]
}
# VPC AZs
output "azs" {
  description = "A list of availability zones spefified as argument to this module"
  value       = [aws_subnet.public[0].availability_zone, aws_subnet.public[1].availability_zone]
}

output "subnet_ids" {
  //value = [aws_subnet.public[0].id, aws_subnet.public[1].id, aws_subnet.private[0].id, aws_subnet.private[1].id]
  value = concat(aws_subnet.public[*].id, aws_subnet.private[*].id)
}

output "priv_subnet_ids" {
  //value = [aws_subnet.private[0].id, aws_subnet.private[1].id]
  value = [for subnet in aws_subnet.private : subnet.id]
}
