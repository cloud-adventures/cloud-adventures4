module "network_module" {
  source = "./modules/network"
}

module "eks_module" {
  source          = "./modules/eks"
  subnet_ids      = module.network_module.subnet_ids
  priv_subnet_ids = module.network_module.priv_subnet_ids

}
