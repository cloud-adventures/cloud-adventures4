output "vpc_azs" {
  value       = module.network_module.azs
  description = "The azs"
}

output "vpc_private_subnets" {
  value       = module.network_module.private_subnets
  description = "private subnets"
}
output "vpc_public_subnets" {
  value       = module.network_module.public_subnets
  description = "private subnets"
}

output "subnet_ids" {
  value       = module.network_module.subnet_ids
  description = "ids"
}

output "priv_ids" {
  value = module.network_module.priv_subnet_ids
}
