# Cloud Computing 4
<figure align = "center">
<img src="images/cloud4.png" alt="Trulli" width="800" height="423"">
</figure>

## Name
Kuberbetes Deployment CI/CD Pipeline. 

## Description
Projects takes ideas from online courses, websites and articles to produce a working application.  It:
- Creates Kubernetes Cluster using Terraform
- Carries out stages; test, build and deploy in GitLab Pipeline
- Deploys simple microservice to the Kubernetes cluster.

The project utilises:
- AWS hosted runners for processing stages
- Gitlab Container registry push and pull of images
- Automatic versioning of images
- Pipeline artifacts
- Kubernetes manifests for creating services and deployments

### Folders
- `app` contains a simple web page app.
- `deploy` contains the Terraform modules which create the AWS Architecture.  Should be running prior to pipeline
-  `k8s` contains kubenetes manifests, namely loadbalancer service and a deployment which deploys the app pod(s)

### GitLab CI/CD

<figure align = "center">
<img src="images/Icons_800x400-02.png" alt="app_folder" width="800" height="400"">
<figcaption align = "center"><b>CI/CD Pipeline</b></figcaption>
</figure>

## AWS Architecture

<figure align = "center">
<img src="images/cloud5.png" alt="AWS " width="800" height="712"">
<figcaption align = "center"><b>AWS Architecture</b></figcaption>
</figure>


## Usage
After deploying simple test page is displayed on the Loadbalancer arn.  This arn can be found in AWS console or by using command `kubectl get svc` to see the address.


## Contributing
- [GitLab Registry](https://chris-vermeulen.com/using-gitlab-registry-with-kubernetes/)
- [GitLab](https://myops.medium.com/simple-deployment-on-kubernetes-aws-eks-with-gitlab-and-gitlab-ci-50f846434310)
- [EKS Terraform](https://antonputra.com/terraform/how-to-create-eks-cluster-using-terraform/#deploy-eks-cluster-autoscaler)
- [Gitlab Crash Course](https://www.youtube.com/watch?v=qP8kir2GUgo&ab_channel=TechWorldwithNana)
- [Gitlab Tutorial](https://www.youtube.com/watch?v=mnYbOrj-hLY&ab_channel=TechandBeyondWithMoss)



## Project status
Project is ongoing:
 - Add DNS name to loadbalancer using Route53
